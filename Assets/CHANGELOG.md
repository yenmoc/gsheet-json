# Changelog

## [0.0.2] - 23-08-2019

* Upgrade dependency utility to v0.0.3

## [0.0.1] - 11-08-2019

* Initial version

### Features

* Google sheet parse to json
