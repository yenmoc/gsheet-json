﻿/*
 * Author: Trung Dong
 * www.trung-dong.com
 * Last update: 2018/01/21
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
*/

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System;
using System.Net;
using System.IO;
using System.Linq;
using System.Threading;
using UnityModule.Utility;
using UnityModule.Serialize;

// ReSharper disable once CheckNamespace
public class GSpreadSheetsToJson : EditorWindow
{
    private const string CLIENT_ID = "871414866606-7b9687cp1ibjokihbbfl6nrjr94j14o8.apps.googleusercontent.com";

    private const string CLIENT_SECRET = "zF_J3qHpzX5e8i2V-ZEvOdGV";

    private static readonly string[] Scopes = {SheetsService.Scope.SpreadsheetsReadonly};

    /// <summary>
    /// Key of the spreadsheet. Get from url of the spreadsheet.
    /// </summary>
    [SerializeField] private string spreadSheetKey = "";

    /// <summary>
    /// List of sheet names which want to download and convert to json file
    /// </summary>
    [SerializeField] private List<string> wantedSheetNames = new List<string>();

    /// <summary>
    /// Name of application.
    /// </summary>
    private const string APP_NAME = "Unity";

    ///// <summary>
    ///// The root of spreadsheet's url.
    ///// </summary>
    //private string _urlRoot = "https://spreadsheets.google.com/feeds/spreadsheets/";

    /// <summary>
    /// The directory which contain json files.
    /// </summary>
    /// <summary>
    /// The data types which is allowed to convert from sheet to json object
    /// </summary>
    private static readonly List<string> AllowedDataTypes = new List<string>()
    {
        "string",
        "int",
        "bool",
        "float",
        "string[]",
        "int[]", "bool[]",
        "float[]",
        "KeyValue<string,string>",
        "KeyValue<string,int>",
        "KeyValue<int,int>",
        "KeyValue<int,string>",
        "KeyValue<string,string,int>",
        "KeyValue<string,int,int>",
        "KeyValue<int,int,string>",
        "KeyValue<int,string,string>",
        "KeyValue<string,int,string>",
        "KeyValue<int,string,int>",
    };
    
    private const string KEY_SAVE_SHEET = "spread_sheet_key_editor";

    /// <summary>
    /// Position of the scroll view.
    /// </summary>
    private Vector2 _scrollPosition;

    /// <summary>
    /// Progress of download and convert action. 100 is "completed".
    /// </summary>
    private float _progress = 100;

    /// <summary>
    /// The message which be shown on progress bar when action is running.
    /// </summary>
    private string _progressMessage = "";
    
    [MenuItem("Utility/GSheet to Json")]
    private static void ShowWindow()
    {
        GetWindow(typeof(GSpreadSheetsToJson));
    }

    private void Init()
    {
        _progress = 100;
        _progressMessage = "";
        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
    }

    private void OnGUI()
    {
        Init();

        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUI.skin.scrollView);
        GUILayout.BeginVertical();
        {
            GUILayout.Label("Settings", EditorStyles.boldLabel);
            if (PlayerPrefs.HasKey(KEY_SAVE_SHEET) && spreadSheetKey.IsNullOrEmpty())
            {
                spreadSheetKey = PlayerPrefs.GetString(KEY_SAVE_SHEET);
            }

            spreadSheetKey = EditorGUILayout.TextField("Spread sheet key", spreadSheetKey);

            GUILayout.Label("");
            GUILayout.Label("Sheet names", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("These sheets below will be downloaded. Let the list blank (remove all items) if you want to download all sheets", MessageType.Info);

            var removeId = -1;
            for (var i = 0; i < wantedSheetNames.Count; i++)
            {
                GUILayout.BeginHorizontal();
                wantedSheetNames[i] = EditorGUILayout.TextField($"Sheet {i}", wantedSheetNames[i]);
                if (GUILayout.Button("X", EditorStyles.toolbarButton, GUILayout.Width(20)))
                {
                    removeId = i;
                }

                GUILayout.EndHorizontal();
            }

            if (removeId >= 0)
                wantedSheetNames.RemoveAt(removeId);
            GUILayout.Label(wantedSheetNames.Count <= 0 ? "Download all sheets" : $"Download {wantedSheetNames.Count} sheets");

            if (GUILayout.Button("Add sheet name", GUILayout.Width(130)))
            {
                wantedSheetNames.Add("");
            }

            GUILayout.Label("");
            GUI.backgroundColor = UnityEngine.Color.green;
            if (GUILayout.Button("Download data \nthen convert to Json files"))
            {
                _progress = 0;
                DownloadToJson();
                PlayerPrefs.SetString(KEY_SAVE_SHEET, spreadSheetKey);
            }

            GUI.backgroundColor = UnityEngine.Color.white;
            if ((_progress < 100) && (_progress > 0))
            {
                if (EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100))
                {
                    _progress = 100;
                    EditorUtility.ClearProgressBar();
                }
            }
            else
            {
                EditorUtility.ClearProgressBar();
            }
        }
        try
        {
            GUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }
        catch (Exception)
        {
            //Sometimes, Unity fire a "InvalidOperationException: Stack empty." bug when Editor want to end a group layout
        }
    }

    private void DownloadToJson()
    {
        //Validate input
        if (string.IsNullOrEmpty(spreadSheetKey))
        {
            Debug.LogError("spreadSheetKey can not be null!");
            return;
        }

        Debug.Log("Start downloading from key: " + spreadSheetKey);

        //Authenticate
        _progressMessage = "Authenticating...";
        var service = new SheetsService(new BaseClientService.Initializer()
        {
            HttpClientInitializer = GetCredential(),
            ApplicationName = APP_NAME,
        });

        _progress = 5;
        EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);
        _progressMessage = "Get list of spreadsheets...";
        EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);

        var spreadSheetData = service.Spreadsheets.Get(spreadSheetKey).Execute();
        var sheets = spreadSheetData.Sheets;


        //if((feed == null)||(feed.Entries.Count <= 0))
        if ((sheets == null) || (sheets.Count <= 0))
        {
            Debug.LogError("Not found any data!");
            _progress = 100;
            EditorUtility.ClearProgressBar();
            return;
        }

        _progress = 15;

        //For each sheet in received data, check the sheet name. If that sheet is the wanted sheet, add it into the ranges.
        var ranges = new List<string>();
        foreach (var sheet in sheets)
        {
            if ((wantedSheetNames.Count <= 0) || (wantedSheetNames.Contains(sheet.Properties.Title)))
            {
                ranges.Add(sheet.Properties.Title);
            }
        }

        var request = service.Spreadsheets.Values.BatchGet(spreadSheetKey);
        request.Ranges = ranges;
        var response = request.Execute();

        //For each wanted sheet, create a json file
        foreach (var valueRange in response.ValueRanges)
        {
            var sheetname = valueRange.Range.Split('!')[0];
            _progressMessage = $"Processing {sheetname}...";
            EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);
            //Create json file
            CreateJsonFile(sheetname, valueRange);
            if (wantedSheetNames.Count <= 0)
                _progress += 85f / (response.ValueRanges.Count);
            else
                _progress += 85f / wantedSheetNames.Count;
        }

        _progress = 100;
        AssetDatabase.Refresh();

        Debug.Log("Download completed.");
    }

    private static void CreateJsonFile(string fileName, ValueRange valueRange)
    {
        //Get properties's name, data type and sheet data
        IDictionary<int, string> propertyNames = new Dictionary<int, string>(); //Dictionary of (column index, property name of that column)
        IDictionary<int, string> dataTypes = new Dictionary<int, string>(); //Dictionary of (column index, data type of that column)
        IDictionary<int, Dictionary<int, string>> values = new Dictionary<int, Dictionary<int, string>>(); //Dictionary of (row index, dictionary of (column index, value in cell))
        var rowIndex = 0;
        foreach (var row in valueRange.Values)
        {
            var columnIndex = 0;
            foreach (string cellValue in row)
            {
                var value = cellValue;
                if (rowIndex == 0)
                {
                    //This row is properties's name row
                    propertyNames.Add(columnIndex, value);
                }
                else if (rowIndex == 1)
                {
                    //This row is properties's data type row
                    dataTypes.Add(columnIndex, value);
                }
                else
                {
                    //Data rows
                    //Because first row is name row and second row is data type row, so we will minus 2 from rowIndex to make data index start from 0
                    if (!values.ContainsKey(rowIndex - 2))
                    {
                        values.Add(rowIndex - 2, new Dictionary<int, string>());
                    }

                    values[rowIndex - 2].Add(columnIndex, value);
                }

                columnIndex++;
            }

            rowIndex++;
        }

        //Create list of Dictionaries (property name, value). Each dictionary represent for a object in a row of sheet.
        var datas = new List<object>();
        foreach (var rowId in values.Keys)
        {
            var thisRowHasError = false;
            var data = new Dictionary<string, object>();
            foreach (var columnId in propertyNames.Keys)
            {
                //Read through all columns in sheet, with each column, create a pair of property(string) and value(type depend on dataType[columnId])
                if (thisRowHasError) break;
                if ((!dataTypes.ContainsKey(columnId)) || (!AllowedDataTypes.Contains(dataTypes[columnId])))
                    continue; //There is not any data type or this data type is strange. May be this column is used for comments. Skip this column.
                if (!values[rowId].ContainsKey(columnId))
                {
                    values[rowId].Add(columnId, "");
                }

                var strVal = values[rowId][columnId];

                switch (dataTypes[columnId])
                {
                    #region string

                    case "string":
                    {
                        data.Add(propertyNames[columnId], strVal);
                        break;
                    }

                    #endregion

                    #region int

                    case "int":
                    {
                        var val = 0;
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            try
                            {
                                val = int.Parse(strVal);
                            }
                            catch (Exception e)
                            {
                                Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                thisRowHasError = true;
                                continue;
                            }
                        }

                        data.Add(propertyNames[columnId], val);
                        break;
                    }

                    #endregion

                    #region bool

                    case "bool":
                    {
                        var val = false;
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            try
                            {
                                val = bool.Parse(strVal);
                            }
                            catch (Exception e)
                            {
                                Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                continue;
                            }
                        }

                        data.Add(propertyNames[columnId], val);
                        break;
                    }

                    #endregion

                    #region float

                    case "float":
                    {
                        var val = 0f;
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            try
                            {
                                val = float.Parse(strVal);
                            }
                            catch (Exception e)
                            {
                                Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                continue;
                            }
                        }

                        data.Add(propertyNames[columnId], val);
                        break;
                    }

                    #endregion

                    #region string[]

                    case "string[]":
                    {
                        var valArr = strVal.Split('&');
                        data.Add(propertyNames[columnId], valArr);
                        break;
                    }

                    #endregion

                    #region int[]

                    case "int[]":
                    {
                        var strValArr = strVal.Split('&');
                        var valArr = new int[strValArr.Length];
                        if (string.IsNullOrEmpty(strVal.Trim()))
                        {
                            valArr = new int[0];
                        }

                        var error = false;
                        for (var i = 0; i < valArr.Length; i++)
                        {
                            var val = 0;
                            if (!string.IsNullOrEmpty(strValArr[i]))
                            {
                                try
                                {
                                    val = int.Parse(strValArr[i]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            valArr[i] = val;
                        }

                        if (error)
                            continue;
                        data.Add(propertyNames[columnId], valArr);
                        break;
                    }

                    #endregion

                    #region bool[]

                    case "bool[]":
                    {
                        var strValArr = strVal.Split('&');
                        var valArr = new bool[strValArr.Length];
                        if (string.IsNullOrEmpty(strVal.Trim()))
                        {
                            valArr = new bool[0];
                        }

                        var error = false;
                        for (var i = 0; i < valArr.Length; i++)
                        {
                            var val = false;
                            if (!string.IsNullOrEmpty(strValArr[i]))
                            {
                                try
                                {
                                    val = bool.Parse(strValArr[i]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            valArr[i] = val;
                        }

                        if (error)
                            continue;
                        data.Add(propertyNames[columnId], valArr);
                        break;
                    }

                    #endregion

                    #region float[]

                    case "float[]":
                    {
                        var strValArr = strVal.Split('&');
                        var valArr = new float[strValArr.Length];
                        if (string.IsNullOrEmpty(strVal.Trim()))
                        {
                            valArr = new float[0];
                        }

                        var error = false;
                        for (var i = 0; i < valArr.Length; i++)
                        {
                            var val = 0f;
                            if (!string.IsNullOrEmpty(strValArr[i]))
                            {
                                try
                                {
                                    val = float.Parse(strValArr[i]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            valArr[i] = val;
                        }

                        if (error)
                            continue;
                        data.Add(propertyNames[columnId], valArr);
                        break;
                    }

                    #endregion

                    #region KeyValue<string,string>

                    case "KeyValue<string,string>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<string, string>>();
                        var lengthValArr = strValArr.Length;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<string, string> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                var temp = strValArr[i].Split(':');
                                val = new KeyValue<string, string>(temp[0], temp[1]);
                            }

                            result.Add(val);
                        }

                        data.Add(propertyNames[columnId], result);

                        break;
                    }

                    #endregion

                    #region KeyValue<int,int>

                    case "KeyValue<int,int>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<int, int>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<int, int> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':').Select(int.Parse).ToArray();
                                    val = new KeyValue<int, int>(temp[0], temp[1]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);

                        break;
                    }

                    #endregion

                    #region KeyValue<string,int>

                    case "KeyValue<string,int>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<string, int>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<string, int> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<string, int>(temp[0], int.Parse(temp[1]));
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);

                        break;
                    }

                    #endregion

                    #region KeyValue<string,int,string>

                    case "KeyValue<string,int,string>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<string, int, string>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<string, int, string> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<string, int, string>(temp[0], int.Parse(temp[1]), temp[2]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    #region KeyValue<int,string>

                    case "KeyValue<int,string>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<int, string>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<int, string> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<int, string>(int.Parse(temp[0]), temp[1]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);

                        break;
                    }

                    #endregion


                    #region KeyValue<string,string,int>

                    case "KeyValue<string,string,int>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<string, string, int>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<string, string, int> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<string, string, int>(temp[0], temp[1], int.Parse(temp[2]));
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    #region KeyValue<string,int,int>

                    case "KeyValue<string,int,int>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<string, int, int>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<string, int, int> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<string, int, int>(temp[0], int.Parse(temp[1]), int.Parse(temp[2]));
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    #region KeyValue<int,int,string>

                    case "KeyValue<int,int,string>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<int, int, string>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<int, int, string> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<int, int, string>(int.Parse(temp[0]), int.Parse(temp[1]), temp[2]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    #region KeyValue<int,string,string>

                    case "KeyValue<int,string,string>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<int, string, string>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<int, string, string> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<int, string, string>(int.Parse(temp[0]), temp[1], temp[2]);
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    #region KeyValue<int,string,int>

                    case "KeyValue<int,string,int>":
                    {
                        var strValArr = strVal.Split('&');
                        var result = new List<KeyValue<int, string, int>>();
                        var lengthValArr = strValArr.Length;
                        var error = false;
                        for (var i = 0; i < lengthValArr; i++)
                        {
                            KeyValue<int, string, int> val = null;
                            if (!strValArr[i].IsNullOrEmpty())
                            {
                                try
                                {
                                    var temp = strValArr[i].Split(':');
                                    val = new KeyValue<int, string, int>(int.Parse(temp[0]), temp[1], int.Parse(temp[2]));
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError($"There is exception when parse value of property {propertyNames[columnId]} of {fileName} class.\nDetail: {e}");
                                    error = true;
                                    break;
                                }
                            }

                            result.Add(val);
                        }

                        if (error)
                        {
                            continue;
                        }

                        data.Add(propertyNames[columnId], result);
                        break;
                    }

                    #endregion

                    // ReSharper disable once RedundantEmptySwitchSection
                    default: break;
                    //This data type is strange, may be this column is used for comments, not for store data, so do nothing and read next column.
                }
            }

            if (!thisRowHasError)
            {
                datas.Add(data);
            }
            else
            {
                Debug.LogError("There's error!");
            }
        }

        //Create json text
        var jsonText = JsonConvert.SerializeObject(datas);

        jsonText.CopyToClipboard();
        Debug.Log("Coppied to clipboard!");
    }

    private UserCredential GetCredential()
    {
        var ms = MonoScript.FromScriptableObject(this);
        var scriptFilePath = AssetDatabase.GetAssetPath(ms);
        var fi = new FileInfo(scriptFilePath);
        var scriptFolder = fi.Directory?.ToString();
        var unused = scriptFolder?.Replace('\\', '/');
        Debug.Log("Save Credential to: " + scriptFolder);

        UserCredential credential = null;
        var clientSecrets = new ClientSecrets {ClientId = CLIENT_ID, ClientSecret = CLIENT_SECRET};
        try
        {
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                clientSecrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(scriptFolder, true)).Result;
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        return credential;
    }

    private static bool MyRemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        var isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        // ReSharper disable once InvertIf
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < chain.ChainStatus.Length; i++)
            {
                // ReSharper disable once InvertIf
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    var chainIsValid = chain.Build((X509Certificate2) certificate);
                    // ReSharper disable once InvertIf
                    if (!chainIsValid)
                    {
                        Debug.LogError("certificate chain is not valid");
                        isOk = false;
                    }
                }
            }
        }

        return isOk;
    }
}